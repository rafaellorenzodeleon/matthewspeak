<?php
/**
 * Template Name: Location
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Matthew\'s_Peak
 */

get_header(); ?>

    <!-- ===========================
		    CUSTOM STYLES DECLARATION
		    ============================ -->
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/inner-page-styles.css' rel='stylesheet' type='text/css'>
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/location-styles.css' rel='stylesheet' type='text/css'>

    <!-- CUSTOM STYLES USING ACF -->
    <style type="text/css">
        <?php the_field('custom_styles', get_option('page_for_posts'));
        ?>
    </style>


    <!-- ===========================
			BANNER
============================ -->
    <div id="header">
        <h1 class="f-awesome-script">location</h1>
    </div>
    <!-- / -->


    <!-- ===========================
		        CONTENT START
		        ============================ -->
    <div id="content-container">
        <img class="main-leaf" src="<?php bloginfo( 'template_url' ); ?>/assets/main-leaf.png">

        <div class="content-wrap">
                <div id="welcome" class="welcome center col-md-12 relative">
                    
                    <?php if( get_field('welcome_text') ): ?>
                        <h3 class="f-playfair">
                            <?php the_field('welcome_text'); ?>
                        </h3>
                    <?php endif; ?>
                    
                    <?php if( get_field('body_text') ): ?>
                        <p class="f-opensans"><?php the_field('body_text'); ?></p>
                    <?php endif; ?>
                    
                    
                   </div>
        </div>
        
        
        <div class="content-wrap">
                    
                    <!-- DRIVING DIRECTIONS -->
                        <div id="direction-details">
                            <ul>



                                <?php if( have_rows('directions') ): ?>
                                <?php while( have_rows('directions') ): the_row(); 
                                        // vars
                                    $title = get_sub_field('title');
                                    $description = get_sub_field('description');
                                    ?>


                                    <!-- FAQ ITEM -->
                                    <li class="transition">
                                        <p class="directions-title f-opensans">
                                            <?php echo $title; ?>

                                            <span class="absolute f-bariol plus">+</span>
                                            <span class="absolute f-bariol minus">-</span>
                                        </p>

                                        <p class="directions-detail f-opensans" style="">
                                            <?php echo $description; ?>
                                        </p>
                                    </li>


                                    <?php endwhile; ?>
                                        <?php endif; ?>
                            </ul>

                        </div>

                    
                    
                    <!-- DOWNLOADABLE MAP -->
                    <?php 
                    $file = get_field('downloadable_map');
                    if( $file ): ?>
                        <a target="_blank" href="<?php echo $file['url']; ?>" class="download-btn f-playfair">Download Vicinity Map</a>
                    <?php endif; ?>
                    
                    
            
        </div>
    </div>

    
    <div class="clear"></div>
            
            
    <!-- ============= 
           MAP 
    ============= -->
    <?php
        $placeId = get_field('place_id', 'option');
        $longtitude = get_field('longtitude', 'option');
        $latitude = get_field('latitude', 'option');
        $zoom = get_field('zoom', 'option');
        $marker = get_field('custom_marker', 'option');
        $maphue = get_field('map_hue', 'option');
        $mapsat = get_field('map_saturation', 'option');
        $roadcolor = get_field('road_fill_color', 'option');
        $roadlabelcolor = get_field('road_labels_color', 'option');
        $generallabelscolor = get_field('general_labels_text_color', 'option');
    ?>
    <div id="map-section" 
         data-place-id="<?php echo $placeId; ?>" 
         data-longtitude="<?php echo $longtitude; ?>" 
         data-latitude="<?php echo $latitude; ?>" 
         data-zoom="<?php echo $zoom; ?>" 
         data-marker="<?php echo $marker['url']; ?>" 
         data-maphue="<?php echo $maphue; ?>" 
         data-saturation="<?php echo $mapsat; ?>" 
         data-roadcolor="<?php echo $roadcolor; ?>" 
         data-roadlabelcolor="<?php echo $roadlabelcolor; ?>" 
         data-generallabelcolor="<?php echo $generallabelscolor; ?>" 
    >
        <div id="map"></div>
    </div>

            
    

    <div class="clear"></div>



    <!-- CUSTOM SCRIPTS USING ACF -->
    <script type="text/javascript">
        <?php the_field('custom_scripts',get_option('page_for_posts')); ?>
    </script>




    <!-- MAP SCRIPTS -->
    <span id="tempId" style="display:none"><?php bloginfo( 'template_url' ); ?></span>
    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/custom-map.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1hmv4Y3se1LgqD_Qfq98XbjxoiEaXVoU&libraries=places&callback=initMap">
    </script>


    <?php get_footer(); ?>