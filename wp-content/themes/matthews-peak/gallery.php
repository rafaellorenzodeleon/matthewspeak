<?php
/**
 * Template Name: Gallery
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Matthew\'s_Peak
 */

get_header(); ?>

    <!-- ===========================
		    CUSTOM STYLES DECLARATION
		    ============================ -->
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/inner-page-styles.css' rel='stylesheet' type='text/css'>
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/gallery-styles.css' rel='stylesheet' type='text/css'>

    <!-- CUSTOM STYLES USING ACF -->
    <style type="text/css">
        <?php the_field('custom_styles', get_option('page_for_posts'));
        ?>
    </style>


    <!-- ===========================
    CUSTOM SCRIPTS DECLARATION
============================ -->
    <script src="<?php bloginfo( 'template_url' ); ?>/js/isotope.pkgd.min.js" type="text/javascript"></script>



    <!-- ===========================
			BANNER
============================ -->
    <div id="header">
        <h1 class="f-awesome-script">Gallery</h1>
    </div>
    <!-- / -->


    <!-- ===========================
		        CONTENT START
		        ============================ -->
    <div id="content-container">
        <img class="main-leaf" src="<?php bloginfo( 'template_url' ); ?>/assets/main-leaf.png">

        <div class="content-wrap">
            <div id="welcome" class="welcome center col-md-12 relative">
                
                <?php if( have_rows('welcome_text') ): ?>
                    <?php while( have_rows('welcome_text') ): the_row(); 
							// vars
						$mainheading = get_sub_field('main_heading');
                        $subheading1 = get_sub_field('sub-heading_1');
                        $subheading2 = get_sub_field('sub-heading_2');
						?>
                            <h3 class="f-playfair"><?php echo $mainheading; ?></h3>
                            <p class="f-opensans center"><?php echo $subheading1; ?></p>
                            <p class="f-opensans center"><?php echo $subheading2; ?></p>
                    <?php endwhile; ?>
                <?php endif; ?>
                
                
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>


        <div class="content-wrap">
            <div id="gallery">

                <ul class="gallery-filters center f-opensans">

                    <?php if( have_rows('filters') ): ?>
                        <?php while( have_rows('filters') ): the_row(); 
                                // vars
                            $title = get_sub_field('title');
                            $class_filter = get_sub_field('class_filter');
                            ?>
                            <li data-filter="<?php echo $class_filter; ?>">
                                <?php echo $title; ?>
                            </li>
                            <?php endwhile; ?>
                                <?php endif; ?>


                </ul>


                <div class="grid">
                    <div class="grid-sizer"></div>
                    <div class="gutter-sizer"></div>

                    <?php if( have_rows('image') ): ?>
                        <?php while( have_rows('image') ): the_row(); 
                                // vars
                            $image = get_sub_field('image');
                            $image_class_filter = get_sub_field('image_class_filter');
                            ?>
                            <div class="grid-item <?php echo $image_class_filter; ?>"><img src="<?php echo $image['url']; ?>"></div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                    
                </div>


                <div class="clear"></div>
            </div>
        </div>


        <div class="clear"></div>
</div>

<div class="clear"></div>
        <!-- CUSTOM SCRIPTS USING ACF -->
        <script type="text/javascript">
            <?php the_field('custom_scripts',get_option('page_for_posts')); ?>
        </script>


        <?php get_footer(); ?>