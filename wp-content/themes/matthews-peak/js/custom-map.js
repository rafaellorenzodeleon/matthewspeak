var main = $('#map-section');
var cm_placeId = main.attr('data-place-id');
var cm_longtitude = parseFloat(main.attr('data-longtitude'));
var cm_latitude = parseFloat(main.attr('data-latitude'));
var cm_zoom = parseInt(main.attr('zoom'));
var cm_marker = main.attr('data-marker');

var cm_maphue = main.attr('data-maphue');
var cm_saturation = parseInt(main.attr('data-saturation'));
var cm_roadcolor = main.attr('data-roadcolor');
var cm_roadlabelcolor = main.attr('data-roadlabelcolor');
var cm_generallabelcolor = main.attr('data-generallabelcolor');

function initMap() {
    var styles = [
        {
            stylers: [
                {
                    hue: cm_maphue
                },
                {
                    saturation: cm_saturation
                }
	      ]
	    },
        {
            "featureType": "landscape",
            "stylers": [
              { "visibility": "on" },
              { "color": cm_maphue }
            ]
          },
        {
            featureType: "road",
            elementType: "geometry.fill",
            stylers: [
                {
                    visibility: "on"
                },
                {
                    color: cm_roadcolor
                }
	      ]
	    }, {
            "featureType": "road",
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": cm_roadlabelcolor
                },
                {
                    "weight": 0.7
                }
    ]
  }, {
            featureType: "all",
            elementType: "labels.text.fill",
            stylers: [
                {
                    visibility: "on"
                },
                {
                    color: cm_generallabelcolor
                }
	      ]
	    }
	  ];

    var styledMap = new google.maps.StyledMapType(styles, {
        name: "Styled Map"
    });

    var image = cm_marker;

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: cm_latitude,
            lng: cm_longtitude
        },
        zoom: 18,
        scrollwheel: false
    });


    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');

    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);

    service.getDetails({
        placeId: cm_placeId
    }, function (place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            var marker = new google.maps.Marker({
                map: map,
                position: place.geometry.location,
                icon: image
            });
            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                    'Place ID: ' + place.place_id + '<br>' +
                    place.formatted_address + '</div>');
                infowindow.open(map, this);
            });
        }
    });
}