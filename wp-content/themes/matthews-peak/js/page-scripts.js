$(document).ready(function () {
    navigationMove();
    
    $('#menu-toggle').click(function () {
        var x = $('.nav-menu').width();
        $('.nav-menu').toggleClass('mobile-nav-open');
        $('.nav-overlay').toggleClass('mobile-nav-open');
        //$('.bodywrap').toggleClass('mobile-nav-open');
        //$('#menu-toggle').toggleClass('mobile-nav-open');
        //$('#main-navigation.fixednav').toggleClass('mobile-nav-open');
    })
    
    $('.nav-overlay').click(function () {
        var x = $('.nav-menu').width();
        $('.nav-menu').toggleClass('mobile-nav-open');
        $('.nav-overlay').toggleClass('mobile-nav-open');
        //$('.bodywrap').toggleClass('mobile-nav-open');
        //$('#menu-toggle').toggleClass('mobile-nav-open');
        //$('#main-navigation.fixednav').toggleClass('mobile-nav-open');
    })
})


$(window).resize(function () {
    navigationMove()
})


function navigationMove(){
    if ($(window).width() > 1023) {
        //$('.nav-menu').removeAttr('style')
        $('.nav-menu').insertAfter($('#mobile-clearer'))
        $('#main-navigation').removeClass('fixednav');
        $('body').removeAttr('style');
        
        $('.nav-menu').removeClass('mobile-nav-open');
        $('.nav-overlay').removeClass('mobile-nav-open');
    }
    
    if ($(window).width() < 1023){
        $('.nav-menu').insertAfter($('.bodywrap'))
        $('#main-navigation').addClass('fixednav');
        $('body').css({
            'padding-top':$('#main-navigation').height()+'px'
        })
        
        $('.nav-menu').removeClass('mobile-nav-open');
        $('.nav-overlay').removeClass('mobile-nav-open');
    }
}