/* ------------------------------------------
BUNNYFISH CREATIVE BROWSER DETECTION SCRIPT 
classes will be added on the body tag to 
distinguish browser or mobile OS

requires - jquery
------------------------------------------ */

var ua = window.navigator.userAgent;
var edge = ua.indexOf("Edge");
var msie = ua.indexOf("MSIE ");
var msie11 = ua.indexOf("Trident/7.0");
var chr = ua.indexOf("Chrome");
var ff = ua.toLowerCase().indexOf('firefox');
var safari = ua.indexOf("Safari");

var userAgent = navigator.userAgent.toLowerCase(),
    browser = '',
    version = 0;


// DETECT IF IOS -- THEN ADD CLASS ios TO BODY
if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) { //document.body.className += " ios"; }
    $('body').addClass('ios');
}
// DETECT IF ANDROID -- THEN ADD CLASS android TO BODY
if (/Android/i.test(navigator.userAgent)) {
    $('body').addClass('android');
}
// DETECT IF BLACKBERRY -- THEN ADD CLASS blackberry TO BODY
if (/BlackBerry/i.test(navigator.userAgent)) {
    $('body').addClass('blackberry');
}
// DETECT IF IE MOBILE THEN ADD ie-mobile CLASS TO BODY
if (/IEMobile/i.test(navigator.userAgent)) {
    $('body').addClass('ie mobile');
}
// DETECT IF OPERA MINI THEN ADD opera-mini CLASS TO BODY
if (/Opera Mini/i.test(navigator.userAgent)) {
    $('body').addClass('opera-mini');
}



// If Internet Explorer
// IE 7 - 10
if (msie > 0) {
    var ieVer = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    //document.body.className += " ie";
    //document.body.className += " ie" + ieVer;
    $('body').addClass('ie');
    $('body').addClass('ie' + ieVer);
    //$('body').addClass('ie').addClass('ie' + ieVer);
}
// -------------
//ie 11
if (msie11 > 0) {
    var ieVer = parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    $('body').addClass('ie11')
        // document.body.className += " ie11";
}

if (edge > -1) {
    //document.body.className += " edge";
    $('body').addClass('edge');
}
//detect chrome
if (chr > 0) {
    $('body').addClass('chrome');
}

//detect firefox
if (ff > 0) {
    $('body').addClass('firefox');
}


//detect safari
if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
    userAgent = userAgent.substring(userAgent.indexOf('version/') + 8);
    userAgent = userAgent.substring(0, userAgent.indexOf('.'));
    version = userAgent;
    browser = "safari";

    //document.body.className += " " + browser + version;
    $('body').addClass(browser + version);
}

//detect edge
if (edge > -1 && chr > -1) {
    //document.body.classList.remove('chrome')
    $('body').removeClass('chrome');
}