<?php
/**
 * HEADER
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Matthew\'s_Peak
 */

?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">


<!-- GOOGLE FONTS -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,400italic,300,300italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Playfair+Display:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel='stylesheet' type='text/css'>


<!-- FONTS -->
<link href='<?php bloginfo( 'template_url' ); ?>/fonts/awesome-script/stylesheet.css' rel='stylesheet' type='text/css'>
<link href="<?php bloginfo( 'template_url' ); ?>/fonts/bariol/stylesheet.css" rel='stylesheet' type='text/css'>
<link href="<?php bloginfo( 'template_url' ); ?>/fonts/master-of-break/stylesheet.css" rel='stylesheet' type='text/css'>
<link href="<?php bloginfo( 'template_url' ); ?>/fonts/trajan/bold/stylesheet.css" rel='stylesheet' type='text/css'>
<link href="<?php bloginfo( 'template_url' ); ?>/fonts/trajan/regular/stylesheet.css" rel='stylesheet' type='text/css'>


<!-- CSS -->
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/reset.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/bootstrap/bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/hover.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/styles.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/css/navigation-and-footer-styles.css">

<!-- JS -->
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/browser-detection.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/classlist/2014.01.31/classList.min.js"></script>

<!-- SLICK -->
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/slick/slick.js"></script>
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/slick/slick.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo( 'template_url' ); ?>/slick/slick-theme.css">
<!-- / -->

<meta property="og:image" content="http://matthewspeakresort.com/wp-content/uploads/2016/09/matthews-peak-private-resort-share.jpg">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="bodywrap">
	<div id="page" class="site">
		<!--<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'matthews-peak' ); ?></a>-->

		<header id="masthead" class="site-header" role="banner">
	        
			<nav id="main-navigation" class="main-navigation" role="navigation">
				<div class="nav-wrap">
	                <div id="menu-toggle" class="fa fa-bars"></div>
	                <div class="logo">
                        <a href="<?php echo get_permalink( get_page_by_path( 'home' ) ); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/assets/logo.png" alt="Matthew's Peak Logo"></a>
	                </div>

	                <div id="mobile-clearer" class="clear"></div>

	                <?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu','menu_class' => 'nav-menu' ) ); ?>

	                <!--<ul class="nav-menu">
	                    <li><a href="home.html">Home</a></li>
	                    <li><a href="rates.html">Rates &amp; Packages</a></li>
	                    <li><a href="reservation.html">Reservation</a></li>
	                    <li><a href="gallery.html">Gallery</a></li>
	                    <li><a href="location.html">Location</a></li>
	                    <li><a href="faqs.html">FAQs</a></li>
	                    <li><a href="about.html">About</a></li>
	                    <li><a href="contact.html">Contact Us</a></li>
	                </ul>-->
	            </div>
			</nav><!-- #site-navigation -->
		</header><!-- #masthead -->
