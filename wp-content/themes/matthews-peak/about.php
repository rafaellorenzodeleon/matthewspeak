<?php
/**
 * Template Name: About
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Matthew\'s_Peak
 */

get_header(); ?>

    <!-- ===========================
		    CUSTOM STYLES DECLARATION
		    ============================ -->
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/inner-page-styles.css' rel='stylesheet' type='text/css'>
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/about-styles.css' rel='stylesheet' type='text/css'>

    <!-- CUSTOM STYLES USING ACF -->
    <style type="text/css">
        <?php the_field('custom_styles', get_option('page_for_posts'));
        ?>
    </style>


    <!-- ===========================
			BANNER
============================ -->
    <div id="header">
        <h1 class="f-awesome-script">About Us</h1>
    </div>
    <!-- / -->


    <!-- ===========================
		        CONTENT START
		        ============================ -->
    <div id="content-container">
            <img class="main-leaf" src="<?php bloginfo( 'template_url' ); ?>/assets/main-leaf.png">

            <div class="content-wrap">
                <div id="welcome" class="welcome center col-md-12 relative">
                    
                    <?php if( get_field('welcome_text') ): ?>
                        <h3 class="f-playfair">
                            <?php the_field('welcome_text'); ?>
                        </h3>
                    
                    <?php endif; ?>
                    
                    <?php if( get_field('welcome_sub-heading') ): ?>
                        <p class="f-opensans center">
                            <?php the_field('welcome_sub-heading'); ?>
                        </p>
                    <?php endif; ?>
                    
                    
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>


            <div class="content-wrap">
                <div id="about-details">
                    <div class="col-md-6 col-sm-6">

                        <?php 
                        $main_image = get_field('main_image');
                        if( $main_image ): ?>
                            <img src="<?php echo $main_image['url']; ?>">
                        <?php endif; ?>
                        
                        
                    </div>
                    <div class="col-md-6 col-sm-6">
                        
                        
                        <?php if( have_rows('content_text') ): ?>
                        <?php while( have_rows('content_text') ): the_row(); 
                                // vars
                            $content = get_sub_field('content_item');
                            ?>
                        
                    
                            <p class="f-opensans">
                                <?php echo $content; ?>
                            </p>
                    
                        
                            <?php endwhile; ?>
                                <?php endif; ?>
                        
                    </div>

                    <div class="clear"></div>
                </div>
            </div>

            <div class="clear"></div>

            <div class="content-wrap">
                <div id="about-quote">
                    
                    
                    <?php 
                        $quotation_text = get_field('quote');
                        if( $quotation_text ): ?>
                            <h3 class="f-playfair center">
                                <?php echo $quotation_text; ?>
                            </h3>
                    <?php endif; ?>
                    
                </div>
            </div>

            <div class="clear"></div>
        
            
        <!-- CUSTOM SCRIPTS USING ACF -->
        <script type="text/javascript">
            <?php the_field('custom_scripts',get_option('page_for_posts')); ?>
        </script>


        <?php
		        get_footer();
		        ?>