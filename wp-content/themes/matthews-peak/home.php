<?php
/**
 * Template Name: Home
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Matthew\'s_Peak
 */

get_header(); ?>
    

    <!-- ===========================
		    CUSTOM STYLES DECLARATION
		    ============================ -->
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/home-styles.css' rel='stylesheet' type='text/css'>
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/home-responsive.css' rel='stylesheet' type='text/css'>

    <!-- CUSTOM STYLES USING ACF -->
    <style type="text/css">
        <?php the_field('custom_styles', get_option('page_for_posts'));
        ?>
    </style>

    <!-- ===========================
			BANNER
			============================ -->
    <div id="header">

            <?php if( get_field('promo') ): ?>
                <div class="header-wrap">
                    <div id="promo-tag">
                        <?php the_field('promo'); ?>
                    </div>
                </div>
            <?php endif; ?>

        <!-- slides -->
        <div class="slides">


            <?php if( have_rows('home_page_slider') ): ?>
                <?php while( have_rows('home_page_slider') ): the_row(); 
							// vars
						$image = get_sub_field('image');
                        $text = get_sub_field('banner_text');
						?>

                    <div class="slide" style="
						background-image:url('<?php echo $image['url']; ?>');
						background-size:cover;
						background-position:center center;
						background-repeat:no-repeat;">
                    
                        <div class="banner-title">
                            <h1><?php echo $text; ?></h1>
                            <a href="<?php echo get_permalink( get_page_by_path( 'reservation' ) ); ?>">Make a Reservation</a>
                        </div>
                        
                    </div>


                    <?php endwhile; ?>
                        <?php endif; ?>


        </div>

    </div>
    <!-- / -->


    <!-- ===========================
		        CONTENT START
		        ============================ -->
    <div id="content-container">
        <img alt="matthews peak private resort nature decoration" class="main-leaf" src="<?php bloginfo( 'template_url' ); ?>/assets/main-leaf.png">

        <?php echo do_shortcode('[Sassy_Social_Share type="floating" count="1" url="http://matthewspeakresort.com/" total_shares="ON" align="right" right="20" top="20"]') ?>
        
        <?php
            $sassycss = get_field('sassy_styles', 'option');
            $sassyjs = get_field('sassy_scripts', 'option');
        ?>
        
        <style type="text/css"> <?php echo $sassycss; ?></style>
        <script type="text/javascript"><?php echo $sassyjs; ?></script>

        <div class="content-wrap">
            <div id="welcome" class="welcome center col-md-12 relative">
                <?php if( have_rows('welcome_text') ): ?>
                    <?php while( have_rows('welcome_text') ): the_row(); 
							// vars
						$mainheading = get_sub_field('main_heading');
                        $subheading1 = get_sub_field('sub-heading_1');
                        $subheading2 = get_sub_field('sub-heading_2');
						?>
                        <h1 class="center f-awesome-script"><?php echo $mainheading; ?></h1>
                        <h2 class="upper f-trajan-reg"><?php echo $subheading1; ?></h2>
                        <h3 class="f-playfair"><?php echo $subheading2; ?></h3>
                        <?php endwhile; ?>
                            <?php endif; ?>

                                <div id="overlay-wc-home"></div>
                                <img alt="matthews peak private resort image" class="absolute" src="<?php bloginfo( 'template_url' ); ?>/assets/home/home-wc-1.jpg">
            </div>

            <div id="main" class="home-content col-md-12">

                <?php if( get_field('body_text') ): ?>
                    <p class="f-opensans">
                        <?php the_field('body_text'); ?>
                    </p>
                    <?php endif; ?>

                        <div id="overlay-wc-main"></div>
                        <img alt="matthews peak private resort image" class="absolute" src="<?php bloginfo( 'template_url' ); ?>/assets/home/home-main-img2.jpg">
            </div>

            <div id="rates" class="rate-section col-md-12">
                <div id="overlay-wc-rates"></div>
                <!--<img class="absolute" src="assets/home/home-tree.jpg">-->

                <div class="rate-tiles col-md-10 col-md-offset-2 col-xs-12 col-sm-12">
                    
                    <?php if( have_rows('columns') ): ?>
                    <?php while( have_rows('columns') ): the_row(); 
							// vars
						$icon = get_sub_field('icon');
                        $title = get_sub_field('title');
                        $description = get_sub_field('description');
                        $link = get_sub_field('link');
						?>
                    
                        <!-- tile -->
                        <a href="<?php echo $link; ?>" class="tile col-md-4 col-sm-4">
                            <img alt="matthews peak private resort quick icons" src="<?php echo $icon['url']; ?>">
                            <h3 class="upper f-playfair"><?php echo $title; ?></h3>
                            <p class="f-opensans"><?php echo $description; ?></p>
                        </a>
                        <?php endwhile; ?>
                            <?php endif; ?>
                </div>
            </div>
        </div>
    </div>


    <!-- ============= 
    QUICK TOUR / CAPIZ 
    ============= -->
    <div id="quick-tour">

        <div class="baluster-content center absolute">
            <h2 class="f-awesome-script">Quick Tour</h2>
            <a class="f-playfair" href="<?php echo get_permalink( get_page_by_path( 'gallery' ) ); ?>">Take me to the gallery</a>
        </div>

        <div id="mask-gallery"></div>

        <div class="balluster-wrap">
            <img alt="matthews peak private resort balluster" class="baluster-left" src="<?php bloginfo( 'template_url' ); ?>/assets/home/home-balluster2.png">
            <div class="baluster-bg-wrap"></div>
            <img alt="matthews peak private resort balluster" class="baluster-right" src="<?php bloginfo( 'template_url' ); ?>/assets/home/home-balluster2.png">
        </div>

    </div>


    <!-- ============= 
        CONTACT ROW 
    ============= -->
    <div id="contact-row" class="relative">
        <div class="contact-row-wrap relative">
            <div class="col-md-4">
                <p class="col-md-12 contact-label f-opensans upper">Contact our Manila Office:</p>
                <p class="contact-detail f-lora">
                    <span class="col-md-6"><?php echo get_option('phone-1',$default) ?></span>
                    <span class="col-md-6"><?php echo get_option('phone-2',$default) ?></span>
                </p>
            </div>

            <div class="col-md-4">
                <p class="col-md-12 contact-label f-opensans upper">Mobile Numbers:</p>
                <p class="contact-detail f-lora">
                    <span class="col-md-6"><?php echo get_option('mobile-1',$default) ?></span>
                    <span class="col-md-6"><?php echo get_option('mobile-2',$default) ?></span>
                </p>
            </div>

            <div class="col-md-4">
                <p class="col-md-12 contact-label f-opensans upper">Email Us:</p>
                <p class="contact-detail f-lora">
                    <span class="col-md-5"><a class="mailtolink" href="mailto:<?php echo get_option('email',$default) ?>"><?php echo get_option('email',$default) ?></a></span>
                </p>
            </div>


            <img alt="matthews peak private resort nature decor" src="<?php bloginfo( 'template_url' ); ?>/assets/home/home-leaf-3.png" class="contact-leaf-left absolute">
            <img alt="matthews peak private resort nature decor" src="<?php bloginfo( 'template_url' ); ?>/assets/home/home-leaf-2.png" class="contact-leaf-right absolute">
        </div>
    </div>


    <!-- ============= 
           MAP 
    ============= -->
    <?php
        $placeId = get_field('place_id', 'option');
        $longtitude = get_field('longtitude', 'option');
        $latitude = get_field('latitude', 'option');
        $zoom = get_field('zoom', 'option');
        $marker = get_field('custom_marker', 'option');
        $maphue = get_field('map_hue', 'option');
        $mapsat = get_field('map_saturation', 'option');
        $roadcolor = get_field('road_fill_color', 'option');
        $roadlabelcolor = get_field('road_labels_color', 'option');
        $generallabelscolor = get_field('general_labels_text_color', 'option');
    ?>
    <div id="map-section" 
         data-place-id="<?php echo $placeId; ?>" 
         data-longtitude="<?php echo $longtitude; ?>" 
         data-latitude="<?php echo $latitude; ?>" 
         data-zoom="<?php echo $zoom; ?>" 
         data-marker="<?php echo $marker['url']; ?>" 
         data-maphue="<?php echo $maphue; ?>" 
         data-saturation="<?php echo $mapsat; ?>" 
         data-roadcolor="<?php echo $roadcolor; ?>" 
         data-roadlabelcolor="<?php echo $roadlabelcolor; ?>" 
         data-generallabelcolor="<?php echo $generallabelscolor; ?>" 
    >
        <div id="map"></div>
    </div>


    <!-- CUSTOM SCRIPTS USING ACF -->
    <script type="text/javascript">
        <?php the_field('custom_scripts',get_option('page_for_posts')); ?>
    </script>


    <!-- MAP SCRIPTS -->
    <script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/custom-map.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD1hmv4Y3se1LgqD_Qfq98XbjxoiEaXVoU&libraries=places&callback=initMap">
    </script>


    <?php
		        get_footer();
		        ?>