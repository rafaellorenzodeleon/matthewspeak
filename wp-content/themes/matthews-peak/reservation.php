<?php
/**
 * Template Name: Reservation
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Matthew\'s_Peak
 */

get_header(); ?>


<!-- ===========================
    CUSTOM STYLES DECLARATION
============================ -->
        <link href='<?php bloginfo( 'template_url' ); ?>/css/bootstrap-notcustom/bootstrap.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/inner-page-styles.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/reservation-styles.css' rel='stylesheet' type='text/css'>
        <link href='<?php bloginfo( 'template_url' ); ?>/css/datepicker.css' rel='stylesheet' type='text/css'>


        <!-- ===========================
    CUSTOM SCRIPTS DECLARATION
============================ -->
        <script src="<?php bloginfo( 'template_url' ); ?>/js/bootstrap.js" type="text/javascript"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/moment-with-locales.js" type="text/javascript"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/bootstrap-datetimepicker.js" type="text/javascript"></script>

    <!-- CUSTOM STYLES USING ACF -->
    <style type="text/css">
        <?php the_field('custom_styles', get_option('page_for_posts'));
        ?>
    </style>


    <!-- ===========================
			BANNER
============================ -->
    <div id="header">
        <h1 class="f-awesome-script">Reservation</h1>
    </div>
    <!-- / -->


    <!-- ===========================
		        CONTENT START
		        ============================ -->
    <div id="content-container">
            <img class="main-leaf" src="<?php bloginfo( 'template_url' ); ?>/assets/main-leaf.png">

            <div class="content-wrap">
                <div id="welcome" class="welcome center col-md-12 relative">
                    
                    <?php 
                        $welcome_text = get_field('welcome_text');
                        if( $welcome_text ): ?>
                            <h3 class="f-playfair">
                                <?php echo $welcome_text; ?>
                            </h3>
                    <?php endif; ?>
                    
                    
                    <?php 
                        $welcome_paragraph = get_field('welcome_paragraph');
                        if( $welcome_paragraph ): ?>
                            <p class="f-opensans">
                                <?php echo $welcome_paragraph; ?>
                            </p>
                    <?php endif; ?>
                                        
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
</div>

            <div class="clear line"></div>


            <div class="content-wrap">
                <div id="reservation-details">
                    <div class="col-md-12">
                        <?php echo do_shortcode('[contact-form-7 id="147" title="Reservation Form"]') ?>
                    </div>
                </div>
            </div>
            


            <div class="clear"></div>

        <!-- CUSTOM SCRIPTS USING ACF -->
        <script type="text/javascript">
            <?php the_field('custom_scripts',get_option('page_for_posts')); ?>
        </script>


        <!-- THANK YOU POPUP -->
        <div class="thankyou">
            <div class="thankyouwrap">
                
                <?php 
                        $form_success_thank_you_message_header = get_field('form_success_thank_you_message_header');
                        if( $form_success_thank_you_message_header ): ?>
                            
                                <h3 class="f-awesome-script">
                                    <?php echo $form_success_thank_you_message_header; ?>
                </h3>
                            
                    <?php endif; ?>
                
                <?php 
                        $form_success_thank_you_message = get_field('form_success_thank_you_message');
                        if( $form_success_thank_you_message ): ?>
                            
                                <p class="f-opensans">
                                    <?php echo $form_success_thank_you_message; ?>
                </p>
                            
                    <?php endif; ?>
                
                
                <span class="f-bariol">x</span>
            </div>
        </div>


        <?php get_footer(); ?>


