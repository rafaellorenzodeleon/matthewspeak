<?php
/**
 * Template Name: Faqs
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Matthew\'s_Peak
 */

get_header(); ?>

    <!-- ===========================
		    CUSTOM STYLES DECLARATION
		    ============================ -->
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/inner-page-styles.css' rel='stylesheet' type='text/css'>
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/faqs-styles.css' rel='stylesheet' type='text/css'>

    <!-- CUSTOM STYLES USING ACF -->
    <style type="text/css">
        <?php the_field('custom_styles', get_option('page_for_posts'));
        ?>
    </style>


    <!-- ===========================
			BANNER
============================ -->
    <div id="header">
        <h1 class="f-awesome-script">Frequently<br>asked Questions</h1>
    </div>
    <!-- / -->


    <!-- ===========================
		        CONTENT START
		        ============================ -->
    <div id="content-container">
            <img class="main-leaf" src="<?php bloginfo( 'template_url' ); ?>/assets/main-leaf.png">

            <div class="content-wrap">
                <div id="welcome" class="welcome center col-md-12 relative">
                    
                    
                     <?php if( have_rows('welcome_text') ): ?>
                        <?php while( have_rows('welcome_text') ): the_row(); 
                                // vars
                            $mainheading = get_sub_field('main_heading');
                            $subheading1 = get_sub_field('sub-heading_1');
                            ?>
                        
                    
                            <h3 class="f-playfair"><?php echo $mainheading; ?></h3>
                            <p class="f-opensans center"><?php echo $subheading1; ?></p>

                        
                         <?php endwhile; ?>
                      <?php endif; ?>
                    
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>
    </div>


            <div class="clear"></div>
            
        
            <div class="content-wrap">
                <div id="faq-details">
                    <ul>
                        
                        

                        <?php if( have_rows('faq_item') ): ?>
                        <?php while( have_rows('faq_item') ): the_row(); 
                                // vars
                            $title = get_sub_field('title');
                            $description = get_sub_field('description');
                            ?>
                        
                        
                            <!-- FAQ ITEM -->
                            <li class="transition">
                                <p class="faq-title f-opensans">
                                    <?php echo $title; ?>

                                    <span class="absolute f-bariol plus">+</span>
                                    <span class="absolute f-bariol minus">-</span>
                                </p>

                                <p class="faq-detail f-opensans" style="">
                                    <?php echo $description; ?>
                                </p>
                            </li>
                    
                        
                            <?php endwhile; ?>
                                <?php endif; ?>
                    </ul>
                
                </div>
            </div>

            


            <div class="clear"></div>

        <!-- CUSTOM SCRIPTS USING ACF -->
        <script type="text/javascript">
            <?php the_field('custom_scripts',get_option('page_for_posts')); ?>
        </script>


        <?php
		        get_footer();
		        ?>