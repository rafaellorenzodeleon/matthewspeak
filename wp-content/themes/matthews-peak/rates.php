<?php
/**
 * Template Name: Rates
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Matthew\'s_Peak
 */

get_header(); ?>

    <!-- ===========================
		    CUSTOM STYLES DECLARATION
		    ============================ -->
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/inner-page-styles.css' rel='stylesheet' type='text/css'>
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/rates-styles.css' rel='stylesheet' type='text/css'>

    <!-- CUSTOM STYLES USING ACF -->
    <style type="text/css">
        <?php the_field('custom_styles', get_option('page_for_posts'));
        ?>
    </style>



    <!-- ===========================
			BANNER
============================ -->
    <div id="header">
        <h1 class="f-awesome-script">Rates &amp; <br>Packages</h1>
    </div>
    <!-- / -->


    <!-- ===========================
		        CONTENT START
		        ============================ -->
    <div id="content-container">
        <img class="main-leaf" src="<?php bloginfo( 'template_url' ); ?>/assets/main-leaf.png">

        <div class="content-wrap">
            <div id="welcome" class="welcome center col-md-12 relative">
                
                <?php if( have_rows('welcome_text') ): ?>
                    <?php while( have_rows('welcome_text') ): the_row(); 
							// vars
						$mainheading = get_sub_field('main_heading');
                        $subheading1 = get_sub_field('sub-heading_1');
                        $subheading2 = get_sub_field('sub-heading_2');
						?>
                
                        <h3 class="f-playfair"><?php echo $mainheading; ?></h3>
                        <p class="f-opensans"><?php echo $subheading1; ?></p>
                
                    <?php endwhile; ?>
                <?php endif; ?>
                
                <div class="seebelow">
                    <p class="f-opensans">
                        <b>See our packages below</b>
                        <div class="triangle"></div>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="clear"></div>

    <!-- PACKAGES -->
    <div id="packages">
        <div class="package-wrapper">
            <div id="package-info" class="absolute">
                <div class="package-info-inner" class="relative">

                    <!-- TITLES -->
                    <h3 id="pt-matthew" class="package-title f-awesome-script">Matthew's<br>Package</h3>
                    <h3 id="pt-attic" class="package-title f-awesome-script" style="display:none;">Attic<br>Package</h3>
                    <h3 id="pt-cottage" class="package-title f-awesome-script" style="display:none;">Cottage<br>Package</h3>


                    <!-- PACKAGE DETAILS -->
                    <div class="package-detail">
                        <div id="matthew-detail" class="col-md-12 package-details">
                            <div class="icon-wrap center">
                                <img src="<?php bloginfo( 'template_url' ); ?>/assets/rates-and-packages/matthews/icon.png">
                                <h3 class="f-playfair mobile-title">Matthew&#39;s Package</h3>
                            </div>

                            <div class="package-features col-md-12">

                                <ul class="f-opensans col-md-6 col-sm-6 col-xs-6">
                                    <li>Good for up to 25 persons</li>
                                    <li>3 air-conditioned rooms
                                    </li>
                                    <li>3 toilets and bath</li>
                                    <li>Living and dining area
                                        <br> Kitchen with ref, sink and 2 burner cooktop with gas</li>
                                </ul>


                                <ul class="f-opensans col-md-6 col-sm-6 col-xs-6">
                                    <li>Adult &amp; kiddie swimming pool</li>
                                    <li>Toddler wading area</li>
                                    <li>Outdoor shower</li>
                                    <li>Barbeque grilling area</li>
                                    <li>Unlimited use of videoke</li>
                                </ul>


                                <div class="clear"></div>

                                <div class="rate-details">
                                    <div class="p-o">
                                        <a class="f-opensans center active" data-type="off-peak">
                                                OFF-PEAK RATES
                                            </a>

                                        <a class="f-opensans center" data-type="peak">
                                                PEAK SEASON RATES
                                            </a>

                                        <div class="clear"></div>
                                    </div>

                                    <div class="p-o-rates">
                                        <div class="off-peak">
                                            <div class="col-md-7  col-sm-7 col-xs-5 item">
                                                <p class="f-opensans">24 Hours</p>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                <p class="f-opensans"><?php echo get_option('24-hours-off-peak',"Rates and Packages - Matthew\'s Package") ?></p>
                                            </div>

                                            <div class="clear"></div>

                                            <div>
                                                <div class="col-md-7  col-sm-7 col-xs-5 item">
                                                    <p class="f-opensans">12 Hours</p>
                                                </div>
                                                <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                    <p class="f-opensans"><?php echo get_option('12-hours-off-peak',"Rates and Packages - Matthew\'s Package") ?></p>
                                                </div>

                                            </div>

                                            <div class="clear"></div>
                                        </div>


                                        <div class="peak" style="display:none;">
                                            <div class="col-md-7  col-sm-7 col-xs-5 item">
                                                <p class="f-opensans">24 Hours</p>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                <p class="f-opensans"><?php echo get_option('24-hours-peak',"Rates and Packages - Matthew\'s Package") ?></p>
                                            </div>

                                            <div class="clear"></div>

                                            <div>
                                                <div class="col-md-7  col-sm-7 col-xs-5 item">
                                                    <p class="f-opensans">12 Hours</p>
                                                </div>
                                                <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                    <p class="f-opensans"><?php echo get_option('12-hours-peak',"Rates and Packages - Matthew\'s Package") ?></p>
                                                </div>

                                            </div>

                                            <div class="clear"></div>
                                        </div>


                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>




                        <div id="attic-detail" class="col-md-12 package-details" style="display:none;">
                            <div class="icon-wrap center"><img src="<?php bloginfo( 'template_url' ); ?>/assets/rates-and-packages/attic/icon.png">
                                <h3 class="f-playfair mobile-title">Attic Package</h3>
                            </div>

                            <div class="package-features col-md-12">

                                <ul class="f-opensans col-md-6 col-sm-6 col-xs-6">
                                    <li>Good for up to 10 persons</li>
                                    <li>1 air-conditioned room
                                        <br>(2nd floor)
                                    </li>
                                    <li>1 toilet and bath</li>
                                    <li>Living and dining area
                                        <br> Kitchen with ref, sink and 2 burner cooktop with gas</li>
                                </ul>


                                <ul class="f-opensans col-md-6 col-sm-6 col-xs-6">
                                    <li>Adult &amp; kiddie swimming pool</li>
                                    <li>Toddler wading area</li>
                                    <li>Outdoor shower</li>
                                    <li>Barbeque grilling area</li>
                                    <li>Unlimited use of videoke</li>
                                </ul>


                                <div class="clear"></div>


                                <div class="rate-details">
                                    <div class="p-o">
                                        <a class="f-opensans center active" data-type="off-peak">
                                                OFF-PEAK RATES
                                            </a>

                                        <a class="f-opensans center" data-type="peak">
                                                PEAK SEASON RATES
                                            </a>

                                        <div class="clear"></div>
                                    </div>

                                    <div class="p-o-rates">
                                        <div class="off-peak">
                                            <div class="col-md-7 col-sm-7 col-xs-5 item">
                                                <p class="f-opensans">24 Hours</p>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                <p class="f-opensans"><?php echo get_option('attic-package-24-hours-off-peak',"Rates and Packages - Attic Package") ?></p>
                                            </div>

                                            <div class="clear"></div>

                                            <div>
                                                <div class="col-md-7  col-sm-7 col-xs-5 item">
                                                    <p class="f-opensans">12 Hours</p>
                                                </div>
                                                <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                    <p class="f-opensans"><?php echo get_option('attic-package-12-hours-off-peak',"Rates and Packages - Attic Package") ?></p>
                                                </div>

                                            </div>

                                            <div class="clear"></div>
                                        </div>


                                        <div class="peak" style="display:none;">
                                            <div class="col-md-7 col-sm-7 col-xs-7 col-xs-5 item">
                                                <p class="f-opensans">24 Hours</p>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                <p class="f-opensans"><?php echo get_option('attic-package-24-hours-peak',"Rates and Packages - Attic Package") ?></p>
                                            </div>

                                            <div class="clear"></div>

                                            <div>
                                                <div class="col-md-7 col-sm-7 col-xs-5 item">
                                                    <p class="f-opensans">12 Hours</p>
                                                </div>
                                                <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                    <p class="f-opensans"><?php echo get_option('attic-package-12-hours-peak',"Rates and Packages - Attic Package") ?></p>
                                                </div>

                                            </div>

                                            <div class="clear"></div>
                                        </div>


                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div id="cottage-detail" class="col-md-12 package-details" style="display:none;">
                            <div class="icon-wrap center"><img src="<?php bloginfo( 'template_url' ); ?>/assets/rates-and-packages/cottage/icon.png">
                                <h3 class="f-playfair mobile-title">Cottage Package</h3>
                            </div>

                            <div class="package-features col-md-12">

                                <ul class="f-opensans col-md-6 col-sm-6 col-xs-6">
                                    <li>Good for up to 10 persons</li>
                                    <li>2 air-conditioned rooms
                                        <br>(ground floor)
                                    </li>
                                    <li>3 toilets and bath</li>
                                    <li>Living and dining area
                                        <br> Kitchen with ref, sink and 2 burner cooktop with gas</li>
                                </ul>


                                <ul class="f-opensans col-md-6 col-sm-6 col-xs-6">
                                    <li>Adult &amp; kiddie swimming pool</li>
                                    <li>Toddler wading area</li>
                                    <li>Outdoor shower</li>
                                    <li>Barbeque grilling area</li>
                                    <li>Unlimited use of videoke</li>
                                </ul>


                                <div class="clear"></div>

                                <div class="rate-details">
                                    <div class="p-o">
                                        <a class="f-opensans center active" data-type="off-peak">
                                                OFF-PEAK RATES
                                            </a>

                                        <a class="f-opensans center" data-type="peak">
                                                PEAK SEASON RATES
                                            </a>

                                        <div class="clear"></div>
                                    </div>

                                    <div class="p-o-rates">
                                        <div class="off-peak">
                                            <div class="col-md-7 col-sm-7 col-xs-5 item">
                                                <p class="f-opensans">24 Hours</p>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                <p class="f-opensans"><?php echo get_option('cottage-package-24-hours-off-peak',"Rates and Packages - Cottage Package") ?></p>
                                            </div>

                                            <div class="clear"></div>

                                            <div>
                                                <div class="col-md-7 col-sm-7 col-xs-5 item">
                                                    <p class="f-opensans">12 Hours</p>
                                                </div>
                                                <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                    <p class="f-opensans"><?php echo get_option('cottage-package-12-hours-off-peak',"Rates and Packages - Cottage Package") ?></p>
                                                </div>

                                            </div>

                                            <div class="clear"></div>
                                        </div>


                                        <div class="peak" style="display:none;">
                                            <div class="col-md-7 col-sm-7 col-xs-5 item">
                                                <p class="f-opensans">24 Hours</p>
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                <p class="f-opensans"><?php echo get_option('cottage-package-24-hours-peak',"Rates and Packages - Cottage Package") ?></p>
                                            </div>

                                            <div class="clear"></div>

                                            <div>
                                                <div class="col-md-7 col-sm-7 col-xs-5 item">
                                                    <p class="f-opensans">12 Hours</p>
                                                </div>
                                                <div class="col-md-5 col-sm-5 col-xs-7 item">
                                                    <p class="f-opensans"><?php echo get_option('cottage-package-12-hours-peak',"Rates and Packages - Cottage Package") ?></p>
                                                </div>

                                            </div>

                                            <div class="clear"></div>
                                        </div>


                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>


                    <!-- DOWN BUTTON -->
                    <a class="package-btn absolute fa fa-chevron-down"></a>


                    <!-- PAGINATION -->
                    <ul class="package-pagination absolute">
                        <li><a data-count="0" href="#" class="active transition">M<br>a<br>t<br>t<br>h<br>e<br>w<br>s</a></li>
                        <li><a data-count="1" href="#" class="transition">A<br>t<br>t<br>i<br>c</a></li>
                        <li><a data-count="2" href="#" class="transition">C<br>o<br>t<br>t<br>a<br>g<br>e</a></li>
                    </ul>


                    <img class="absolute" id="down-indicator" src="<?php bloginfo( 'template_url' ); ?>/assets/rates-and-packages/down-arrow.png" />
                </div>
            </div>

            <!-- BGS -->
            <div id="package-background" class="relative transition">
                <div class="package-div matthews"></div>
                <div class="package-div attic"></div>
                <div class="package-div cottage"></div>
            </div>




        </div>

    </div>




    <div class="clear"></div>




    <!-- ADD ONS -->
    <div id="addons">
        <div class="col-md-6 col-sm-6 f-opensans">
            <h3 class="">
                    ADD ONS:
                </h3>
            <ul>
                <li><?php echo get_option('exceeding-guest-allowance-fee',"Rates and Packages - Add Ons") ?> for guests exceeding the allowed number depending on the package availed.</li>
                <li><?php echo get_option('electrical-appliance-fee',"Rates and Packages - Add Ons") ?> for electrical appliance brought in to the resort</li>
            </ul>

            <ul>
                <li>Cost for time extension is <?php echo get_option('cost-for-time-extension',"Rates and Packages - Add Ons") ?></li>
                <li>Check-in/out time will be subject to availability of slot being reserved</li>
                <li>Earliest check-in time for Saturdays is 12NN.</li>
                <li>Early check-in on weekends is allowed at an additional cost.</li>
            </ul>
        </div>

        <div class="col-md-5 col-sm-5 col-md-offset-1 col-sm-offset-1 f-opensans">
            <h3 class="">OFF-PEAK SEASON</h3>
            <ul>
                <li>January 16 - March 14</li>
                <li>June 16 - December 14</li>
            </ul>

            <div class="spacer20"></div>

            <h3 class="">PEAK SEASON</h3>
            <ul>
                <li>March 15 - June 15</li>
                <li>December 15 - January 15 &amp; Holidays</li>
            </ul>
        </div>

        <div class="clear"></div>
    </div>

    <div class="clear"></div>






    <!-- CUSTOM SCRIPTS USING ACF -->
    <script type="text/javascript">
        <?php the_field('custom_scripts',get_option('page_for_posts')); ?>
    </script>


    <?php
		        get_footer();
		        ?>