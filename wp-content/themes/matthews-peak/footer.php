<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Matthew\'s_Peak
 */

?>
	<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/js/page-scripts.js"></script>

	<div id="footer">
            <div class="footer-wrap">
                <div class="footer-item col-md-4">
                    <a href="<?php echo get_permalink( get_page_by_path( 'home' ) ); ?>"><img alt="matthews peak logo" id="footer-logo" src="<?php bloginfo( 'template_url' ); ?>/assets/matthew-peak-logo-white.png" /></a>
                    <div>
                        <span class="fa fa-map-marker"></span>
                        <p class="f-opensans">
                            Barretto Subdivision, <br>
                            Brgy. Pansol, Calamba City,
                            <br> Laguna, Philippines
                        </p>
                    </div>
                </div>

                <div class="footer-item col-md-4">
                    <h3 id="quick-links" class="f-playfair">Quick Links</h3>
                    <ul class="col-md-5">
                        <li class="f-opensans"><a href="<?php echo get_permalink( get_page_by_path( 'about' ) ); ?>">About</a></li>
                        <li class="f-opensans"><a href="<?php echo get_permalink( get_page_by_path( 'rates-packages' ) ); ?>">Rates &amp; Packages</a></li>
                        <li class="f-opensans"><a href="<?php echo get_permalink( get_page_by_path( 'reservation' ) ); ?>">Reservation</a></li>
                        <li class="f-opensans"><a href="<?php echo get_permalink( get_page_by_path( 'gallery' ) ); ?>">Gallery</a></li>
                    </ul>
                    <ul class="col-md-5">
                        <li class="f-opensans"><a href="<?php echo get_permalink( get_page_by_path( 'location' ) ); ?>">Location</a></li>
                        <li class="f-opensans"><a href="<?php echo get_permalink( get_page_by_path( 'faqs' ) ); ?>">FAQs</a></li>
                        <li class="f-opensans"><a href="<?php echo get_permalink( get_page_by_path( 'contact-us' ) ); ?>">Contact Us</a></li>
                    </ul>
                </div>


                <div class="footer-item col-md-4">
                    <h3 id="social" class="f-playfair">Social</h3>
                    <div>
                        <a href="<?php echo get_option('facebook',$default) ?>" target="_blank"><img alt="facebook logo" src="<?php bloginfo( 'template_url' ); ?>/assets/facebook.png"></a>
                        <a href="viber://add?number=<?php echo get_option('viber',$default) ?>"><img alt="viber logo"src="<?php bloginfo( 'template_url' ); ?>/assets/viber.png"></a>
                    </div>

                    <h3 id="other-resort" class="f-playfair">Other Resort</h3>
                    <div>
                        <a class="f-opensans" href="http://www.danielsplaceresort.com" target="_blank">http://www.danielsplaceresort.com</a>
                    </div>
                </div>

            </div>
            
        </div>
<div class="copyright clear row f-opensans">
                <div class="copyright-wrap">
                    <span class="center">
                        Copyright &copy; <?php echo date("Y"); ?> Matthew's Peak Private Resort  &nbsp; &bull; &nbsp; Created with &#9829; by <a href="http://bunnyfishcreatives.com/" target="_blank">bunnyfishcreatives</a>
                    </span>
                </div>
            </div>

<?php wp_footer(); ?>
	</div><!-- END BODY WRAP-->
	<div class="nav-overlay"></div>
</body><!-- END BODY-->
</html>
