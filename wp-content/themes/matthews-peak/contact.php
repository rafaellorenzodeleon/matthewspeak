<?php
/**
 * Template Name: Contact Us
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Matthew\'s_Peak
 */

get_header(); ?>

    <!-- ===========================
		    CUSTOM STYLES DECLARATION
		    ============================ -->
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/inner-page-styles.css' rel='stylesheet' type='text/css'>
    <link href='<?php bloginfo( 'template_url' ); ?>/css/custom/contact-styles.css' rel='stylesheet' type='text/css'>

    <!-- CUSTOM STYLES USING ACF -->
    <style type="text/css">
        <?php the_field('custom_styles', get_option('page_for_posts'));
        ?>
    </style>


    <!-- ===========================
			BANNER
============================ -->
    <div id="header">
        <h1 class="f-awesome-script">Contact Us</h1>
    </div>
    <!-- / -->


    <!-- ===========================
		        CONTENT START
		        ============================ -->
    <div id="content-container">
            <img class="main-leaf" src="<?php bloginfo( 'template_url' ); ?>/assets/main-leaf.png">

            <div class="content-wrap">
                <div id="welcome" class="welcome center col-md-12 relative">
                    
                    <?php 
                        $hero_text = get_field('hero_text');
                        if( $hero_text ): ?>
                            <h3 class="f-playfair">
                                <?php echo $hero_text; ?>
                            </h3>
                    <?php endif; ?>
                    

                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
            </div>

        <div class="clear line"></div>
            <div class="content-wrap">
                <div id="contact-details">
                    <div class="col-md-8 col-sm-8">
                        <?php echo do_shortcode('[contact-form-7 id="139" title="Contact Form"]') ?>
                    </div>





                    <div id="details" class="col-md-4 col-sm-4">
                        <p class="f-opensans">
                            <b>PHONE:</b>
                            <br> <?php echo get_option('phone-1',$default) ?>
                            <br> <?php echo get_option('phone-2',$default) ?>
                        </p>

                        <p class="f-opensans">
                            <b>MOBILE:</b>
                            <br> <?php echo get_option('mobile-1',$default) ?>
                            <br> <?php echo get_option('mobile-2',$default) ?>
                        </p>

                        <p class="f-opensans">
                            <b>EMAIL US AT:</b>
                            <br><a class="mailtolink" href="mailto:<?php echo get_option('email',$default) ?>"><?php echo get_option('email',$default) ?></a>
                        </p>
                    </div>
                </div>

                <div class="clear"></div>
            </div>

            <div class="clear"></div>
</div>
        
            
        <!-- CUSTOM SCRIPTS USING ACF -->
        <script type="text/javascript">
            <?php the_field('custom_scripts',get_option('page_for_posts')); ?>
        </script>


        <?php
		        get_footer();
		        ?>